// @flow
// returns a gaussian random function with the given mean and stdev.
// using https://en.wikipedia.org/wiki/Marsaglia_polar_method
export const gaussian = (mean: number, stdev: number): (() => number) => {
  let y2 = 0;
  let useLast = false;
  return (): number => {
    let y1;
    if (useLast) {
      y1 = y2;
      useLast = false;
    } else {
      let x1 = 0;
      let x2 = 0;
      let w = 0;
      do {
        x1 = 2.0 * Math.random() - 1.0;
        x2 = 2.0 * Math.random() - 1.0;
        w = x1 * x1 + x2 * x2;
      } while (w >= 1.0);
      w = Math.sqrt((-2.0 * Math.log(w)) / w);
      y1 = x1 * w;
      y2 = x2 * w;
      useLast = true;
    }

    const retval = mean + stdev * y1;
    if (retval > 0) {
      return retval;
    } else {
      return -retval;
    }
  };
};

export const randomBetweenMinAndMax = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};
